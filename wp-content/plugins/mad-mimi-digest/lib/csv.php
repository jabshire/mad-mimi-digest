<?php

/* =======================================================
/* Mad Mimi Digest > Subscribers import / export CSV
/* ======================================================= */

function mmdigest_csv_table() {

	if (isset($_POST['import_csv'])) {
		$err_message = false;
		if ($_POST['import_csv']['csv_file'] !== '') {

			$filename = $_POST['import_csv']['csv_file'];

			if (get_post_mime_type($_POST['import_csv']['csv_id']) === 'text/plain') {
				$csv_array = mmdigest_csv_import($filename);
				mmdigest_csv_save_db($csv_array);
			} else {
				$err_message = 'Choose CSV file!';
			}

		} else {
			$err_message = 'Choose CSV file!';
		}
	}

	if(function_exists( 'wp_enqueue_media' )){
	    wp_enqueue_media();
	} else {
	    wp_enqueue_style('thickbox');
	    wp_enqueue_script('media-upload');
	    wp_enqueue_script('thickbox');
	}

?>

<h3>Select the CSV file to import subscribers</h3>

<?php if ($err_message) { 
	echo "<h4>{$err_message}</h4>";
 } ?>

File from the media manager
<div class="uploader">
	<form method="post">
		<input type="text" name="import_csv[csv_file]" id="csv_file" class="regular-text" readonly />
		<input class="button button-primary" id="_unique_name_button" value="Choose CSV file" type="button" /><br><br>
		<input type="text" name="import_csv[csv_id]" id="csv_id" hidden class="regular-text"/>
		<input type="submit" class="button button-primary" value="Import subscribers">
	</form>
</div>
<br>
<h3>Export subscribers to CSV file</h3>
<a href="?bbg_export" class="button button-primary">Export subscribers</a>

<script>
	jQuery(document).ready(function() {	

		var $ = jQuery;

	    var _custom_media = true,
	      _orig_send_attachment = wp.media.editor.send.attachment;

	    $('input[id=_unique_name_button]').click(function(e) {
	    
	      var send_attachment_bkp = wp.media.editor.send.attachment;
	      var button = $(this);
	      _custom_media = true;
	      wp.media.editor.send.attachment = function(props, attachment){
	        if ( _custom_media ) {
	          $("input#csv_file").val(attachment.url);
	          $("input#csv_id").val(attachment.id);
	        } else {
	          return _orig_send_attachment.apply( this, [props, attachment] );
	        };
	      }
	  
	      wp.media.editor.open(button);
	      return false;
	    });
	  
	    $('.add_media').on('click', function(){
	      _custom_media = false;
	    });

	});
</script>

<? }