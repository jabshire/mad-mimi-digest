<?php

add_filter( 'upload_mimes', 'mmdigest_mime_types' );
function mmdigest_mime_types( $mime_types )
{
    $mime_types[] = array(
        'csv' => 'text/plain'
    );
    return $mime_types;
}

/* =======================================================
/* Mad Mimi Digest > Export Subscribers to CSV
/* ======================================================= */

function mmdigest_csv_export() {
	if ( ! is_super_admin() ) {
		return;
	}
 
	if ( ! isset( $_GET['bbg_export'] ) ) {
		return;
	}
 
	$filename = 'mmdigest_export_' . time() . '.csv';
 
	$header_row = array(
		// 0 => 'Email',
		// 1 => 'First name',
		// 2 => 'Last name',
		// 3 => 'Subscription schedule',
		// 4 => 'Create date',
		// 5 => 'Datetime',
		// 6 => 'IP',
		// 7 => 'Active',
		0 => 'email',
		1 => 'first_name',
		2 => 'last_name',
		3 => 'subscription_schedule',
		4 => 'create_date',
		5 => 'datetime',
		6 => 'ip',
		7 => 'active',
	);
 
	$data_rows = array();
 
	global $wpdb, $bp;
	$users = $wpdb->get_results( "SELECT email, first_name, last_name, subscription_schedule, create_date, datetime, ip, active FROM {$wpdb->prefix}mmdigest" );
 
	foreach ( $users as $u ) {
		$row = array();
		$row[0] = $u->email;
		$row[1] = $u->first_name;
		$row[2] = $u->last_name;
		$row[3] = $u->subscription_schedule;
 		$row[4] = $u->create_date;
 		$row[5] = $u->datetime;
 		$row[6] = $u->ip;
 		$row[7] = $u->active;
 
		$data_rows[] = $row;
	}

	header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
	header( 'Content-Description: File Transfer' );
	header( 'Content-type: text/csv' );
	header( "Content-Disposition: attachment; filename={$filename}" );
	header( 'Expires: 0' );
	header( 'Pragma: public' );

	$fh = @fopen( 'php://output', 'w' );
 
	fprintf( $fh, chr(0xEF) . chr(0xBB) . chr(0xBF) );

	fputcsv( $fh, $header_row );
 
	foreach ( $data_rows as $data_row ) {
		fputcsv( $fh, $data_row );
	}
 
	fclose( $fh );

}
add_action( 'admin_init', 'mmdigest_csv_export' );

/* =======================================================
/* Mad Mimi Digest > Import Subscribers from CSV
/* ======================================================= */

function mmdigest_csv_import($filename)
{
    $row = 0;
    $col = 0;
 
    $results = array();

    $handle = @fopen($filename, "r");
    if ($handle) 
    {
        while (($row = fgetcsv($handle, 4096)) !== false) 
        {
            if (empty($fields)) 
            {
                $fields = $row;
                continue;
            }
 
            foreach ($row as $k=>$value) 
            {
                $results[$col][$fields[$k]] = $value;
            }
            $col++;
            unset($row);
        }
        if (!feof($handle)) 
        {
            echo "Error: unexpected fgets() failn";
        }
        fclose($handle);
    }
 
    return $results;
}
add_action( 'admin_init', 'mmdigest_csv_import' );

/* =======================================================
/* Mad Mimi Digest > Save import CSV file to DB
/* ======================================================= */

function mmdigest_csv_save_db($csv_array)
{
	global $wpdb;
	$table_name = $wpdb->prefix . "mmdigest";

	foreach ($csv_array as $csv) {

		$cur_email = $wpdb->get_results("SELECT id FROM {$table_name} WHERE email='".$csv['﻿email']."'");

		if ($cur_email) { } else {
			$wpdb->insert( 
				$table_name, 
				array( 
					'email'		 => $csv['﻿email'], 
					'first_name' => $csv['first_name'], 
					'last_name'	 => $csv['last_name'], 
					'subscription_schedule' => $csv['subscription_schedule'],
					'create_date'=> $csv['create_date'], 
					'datetime'	 => $csv['datetime'], 
					'ip'		 => $csv['ip'], 
					'active'	 => $csv['active'], 
				)
			);
		}

		$wpdb->insert_id;
	}

}
add_action( 'admin_init', 'mmdigest_csv_import' );