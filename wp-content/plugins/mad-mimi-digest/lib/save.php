<?php

/* =======================================================
/* SAVE SETTINGS
/* ======================================================= */

if (isset($_POST['mmd_username']) && isset($_POST['mmd_api'])) {
	
	$config_array = array(
		'username' => $_POST['mmd_username'], 
		'api' => $_POST['mmd_api'], 
		'from' => $_POST['mmd_from'],
		'subject' => $_POST['mmd_subject'],
		'post_type' => $_POST['mmd_post_type'], 
		'promo_daily' => $_POST['mmd_promo_daily'], 
		'promo_weekly' => $_POST['mmd_promo_weekly'],
		'promo_instant' => $_POST['mmd_promo_instant'],
	);
	
	$config = wp_slash(json_encode($config_array));
	
	update_option('mmd_config', $config);
	
	header("Location: ", TRUE, "302");
}

/* =======================================================
/* SAVE SUBSCRIBER
/* ======================================================= */

if (isset($_POST['mmd_subscribe'])) {

	// Get widget options
	$widget_options = get_option('widget_mmd_widget');

	$subscribe = $_POST['mmd_subscribe'];
	$email = $subscribe['email'];

	foreach ($widget_options as $w_option) {

		if ($w_option['show_first_name']) {
			$firstname = $subscribe['firstname'];
		} else {
			$firstname = '';
		}

		if ($w_option['show_last_name']) {
			$lastname = $subscribe['lastname'];
		} else {
			$lastname = '';
		}

		if ($w_option['show_schedule']) {
			$schedule = $subscribe['schedule'];
		} else {
			$schedule = 'daily';
		}

	}

	if (isset($_POST['mmd_subscribe']['custom'])) {
		$audience = $subscribe['audience'];
		$promotion = $subscribe['promotion'];
	} else {
		$audience = (int) $subscribe['audience'];
		$promotion = (int) $subscribe['promotion'];
	}

	// if ($promotion == '0') {
	// 	$promotion = 'no';
	// }

	if ($email && $schedule && $promotion) {

		if (isset($_POST['mmd_subscribe']['custom'])) {
			$audience_name = $audience;
		} else {
			switch($schedule) {
				case 'daily':
					$audience_name = 'Digest Daily';
					break;
				case 'weekly':
					$audience_name = 'Digest Weekly';
					break;
				case 'instant':
					$audience_name = 'Digest Instant';
					break;
				default:
					$audience_name = 'Digest Instant';
					break;
			}
		}

		$config = mmdigest_getConfiguration();
		
		// if (is_numeric($audience)):
		// 	$audience_name = (string) mmdigest_getMadMimiAudienceName($audience);
		// else:
		// 	$audience_name = $audience;
		// endif;
		
		if (is_numeric($promotion)):
			$promotion_name = mmdigest_getMadMimiPromotionName($promotion);
		else:
			$promotion_name = $promotion;
		endif;
			
		$user = array(
			'email'		=> $email, 
			'firstName'	=> $firstname, 
			'lastName'	=> $lastname, 
			'add_list'	=> $audience_name
			); 

		$options = array(   
			'recipients'		=> $firstname .' '. $lastname .' <'. $email .'>', 
			'promotion_name'	=> (string) $promotion_name,
			'subject'			=> $config->subject,
			'from'				=> $config->from
		);

		$mm = new MadMimi($config->username, $config->api, false);
		$mm->AddUser($user);
		$mm->SendMessage($options);
	}

	// add user to wp subscription too
	mmdigest_addUserToWp($email, $schedule, $firstname, $lastname);

}

/* =======================================================
/* SAVE USER TO WP DATABASE
/* ======================================================= */

function mmdigest_addUserToWp($email, $schedule='Instant', $firstname='', $lastname=''){

	global $wpdb;

	$table_name = $wpdb->prefix . "mmdigest";
	$client_ip = get_client_ip();
	
	$user = $wpdb->get_results("SELECT * FROM ". $table_name ." WHERE email='". $email ."'");
	
	if (!$user):
		$wpdb->insert( 
			$table_name, 
			array( 
				'email'		 => $email, 
				'first_name' => $firstname, 
				'last_name'	 => $lastname, 
				'subscription_schedule' => $schedule,
				'create_date'=> time(), 
				'datetime'	 => time(), 
				'ip'		 => $client_ip,
				'active'	 => 1
			)
		);
	endif;
	
}
