<?php

/* =======================================================
/* Mad Mimi Digest > Configuration
/* ======================================================= */

function mmdigest_configuration($config){
	// Get MadMimi promotions
	$mm_promotions = mmdigest_getMadMimiPromotions();
	$requred_field = '<span style="color:red;display:block">This field is requred</span>';

	if ($mm_promotions === 'Unable to authenticate') {
		echo '<h3 style="color:red;display:block">Unable to authenticate your MadMimi account. Please enter valid username and API key MadMimi.</h3>';
	}
	?>
	<hr>
	<h3>Schedule intervals</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label for="mmd_username" style="font-weight:bold;">Daily:</label>
				</th>
				<td>
					<?php echo gmdate("Y-m-d H:i:s", wp_next_scheduled( 'mmdigest_daily_hook' )); ?>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="mmd_username" style="font-weight:bold;">Weekly:</label>
				</th>
				<td>
					<?php echo gmdate("Y-m-d H:i:s", wp_next_scheduled( 'mmdigest_weekly_hook' )); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<hr>
	<h3>Integration</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label for="mmd_username" style="font-weight:bold;">Mad Mimi Username: <span style="color:red">*</span></label>
				</th>
				<td>
					<input name="mmd_username" id="mmd_username" size="40" type="text" value="<?php echo $config->username; ?>">
					<?php if ($config->username === '') { echo $requred_field; } ?>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="mmd_api" style="font-weight:bold;">API Key: <span style="color:red">*</span></label>
				</th>
				<td>
					<input name="mmd_api" id="mmd_api" size="40" type="text" value="<?php echo $config->api; ?>">
					<?php if ($config->api === '') { echo $requred_field; } ?>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="mmd_from" style="font-weight:bold;">From Email: <span style="color:red">*</span></label>
				</th>
				<td>
					<input name="mmd_from" id="mmd_from" size="40" type="text" value="<?php echo $config->from; ?>">
					<?php if ($config->from === '') { echo $requred_field; } ?>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="mmd_subject" style="font-weight:bold;">Subscription Subject: <span style="color:red">*</span></label>
				</th>
				<td>
					<input name="mmd_subject" id="mmd_subject" size="40" type="text" value="<?php echo $config->subject; ?>">
					<?php if ($config->subject === '') { echo $requred_field; } ?>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="mmd_post_type" style="font-weight:bold;">Post type:</label>
				</th>
				<td>
					<select name="mmd_post_type" id="mmd_post_type">
						<option value="full_post" <?php echo ($config->post_type == 'full_post') ? 'selected="selected"':'' ; ?>>
							Full Post
						</option>
						<option value="excerpt_post" <?php echo ($config->post_type == 'excerpt_post') ? 'selected="selected"':'' ; ?>>
							Excerpt
						</option>
						<option value="title_post" <?php echo ($config->post_type == 'title_post') ? 'selected="selected"':'' ; ?>>
							Title Only
						</option>
					</select>
				</td>
			</tr>
			<?php if (($config->username == '' || $config->api == '' || $mm_promotions === 'Unable to authenticate')) {} else { ?>
			<tr>
				<th scope="row">
					<label for="mmd_promo_daily" style="font-weight:bold;">Daily Digest Promotion:</label>
				</th>
				<td>
					<select name="mmd_promo_daily" id="mmd_promo_daily">
					<?php 
					if ($mm_promotions) {
					foreach ($mm_promotions as $mm_promotion):
					?>
					<option value="<?php echo $mm_promotion['id']; ?>" <?php echo ($mm_promotion['id']==$config->promo_daily)? 'selected="selected"':'' ; ?>><?php echo $mm_promotion['name']; ?></option>
					<?php endforeach; } ?>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="mmd_promo_weekly" style="font-weight:bold;">Weekly Digest Promotion:</label>
				</th>
				<td>
					<select name="mmd_promo_weekly" id="mmd_promo_weekly">
					<?php 
					if ($mm_promotions) {
					foreach ($mm_promotions as $mm_promotion):
					?>
					<option value="<?php echo $mm_promotion['id']; ?>" <?php echo ($mm_promotion['id']==$config->promo_weekly)? 'selected="selected"':'' ; ?>><?php echo $mm_promotion['name']; ?></option>
					<?php endforeach; } ?>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="mmd_promo_instant" style="font-weight:bold;">Instant Digest Promotion:</label>
				</th>
				<td>
					<select name="mmd_promo_instant" id="mmd_promo_instant">
					<?php
					if ($mm_promotions) {
					foreach ($mm_promotions as $mm_promotion):
					?>
					<option value="<?php echo $mm_promotion['id']; ?>" <?php echo ($mm_promotion['id']==$config->promo_instant)? 'selected="selected"':'' ; ?>><?php echo $mm_promotion['name']; ?></option>
					<?php endforeach; } ?>
					</select>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
    
   <?php
	
}
