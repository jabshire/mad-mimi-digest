<?php

function mmdigest_shortcode( $atts, $content = null ) {

	ob_start();

	extract(shortcode_atts(array(
		'title'		=> '',
		'audience'	=> '',
		'promotion'	=> '',
		'schedule'	=> '',
		'labels'	=> false,
		'name'		=> false,
		'btn_text'	=> false,
		'class'		=> '',
	), $atts));
	
	// Submit button text
	$submit_btn_text = 'Subscribe';
	if ($btn_text) {
		$submit_btn_text = $btn_text;
	}

	wp_enqueue_style( 'mmdigest', plugins_url( 'mmdigest.css', __FILE__ ), null, '1.0', 'all' );

	$out = '';

	$out .= '<div class="mmd-form mmd-form-post '.$class.'">';
	
	if ($title !== '') {
		$out .= '<h3 class="mmd-form-title">' . $title . '</h3>';
	}
	
	if ($content) {
		$out .= '<div class="mmd-form-description">'. $content .'</div>';
	}
	
	if (isset($_POST['mmd_subscribe'])) {
		
		$out .= '<p class="mmd-subscribed"><strong>You have been subscribed.</strong><br>If a welcome emails does not arrive within 5 minutes, be sure to check in your spam box before contacting.</p>';
		
	} else {
	
		$out .= '<form method="post" id="mmdigest_subscribe">';
		
		$out .= '<div class="mmd-fieldset mmd-form-email">';
		if ($labels) {
			$out .= '<label for="mmd_email">Email <span class="required" title="This field is required">*</span></label>';
		}
		$out .= '<input id="email" name="mmd_subscribe[email]" type="text" value="" placeholder="Email" class="mmd-form-text email" required />';
		$out .= '</div>';
		
		if ($name) {
			$out .= '<div class="mmd-fieldset mmd-form-firstname">';
			$out .= '<label for="mmd_firstname">First name</label>';
			$out .= '<input id="mmd_firstname" name="mmd_subscribe[firstname]" type="text" value="" placeholder="First Name" class="mmd-form-text firstname"/>';
			$out .= '</div>';
			
			$out .= '<div class="mmd-fieldset mmd-form-lastname">';
			$out .= '<label for="mmd_lastname">Last name</label>';
			$out .= '<input id="mmd_lastname" name="mmd_subscribe[lastname]" type="text" value="" placeholder="Last Name" class="mmd-form-text lastname"/>';
			$out .= '</div>';
		}
		
		if ($schedule === '') {
			$out .= '<div class="mmd-fieldset mmd-form-schedule">';
			$out .= '<label for="mmd_schedule">Select schedule</label>';
			$out .= '<select name="mmd_subscribe[schedule]" id="mmd_schedule">';
			$out .= '<option value="daily">Every day</option>';
			$out .= '<option value="weekly">Weekly</option>';
			$out .= '<option value="instant">Instant</option>';
			$out .= '</select>';
			$out .= '</div>';
		}

		$out .= '<div class="mmd-fieldset mmd-form-action">';
		$out .= '<input name="mmd_subscribe[submit]" type="submit" value="' . $submit_btn_text . '" class="button mmd-form-submit submit" />';
		$out .= '</div>';
		
		$out .= '<input name="mmd_subscribe[audience]" type="hidden" value="' . $audience . '" />';
		$out .= '<input name="mmd_subscribe[promotion]" type="hidden" value="' . $promotion . '" />';
		$out .= '<input name="mmd_subscribe[custom]" type="hidden" value="1" />';
	
		if ($schedule !== '') {
			$out .= '<input name="mmd_subscribe[schedule]" type="hidden" value="' . $schedule . '" />';
		}
	
		$out .=	'</form>';
		
	} // endif post
	
	
	$out .= '</div>';

	echo $out;

	return ob_get_clean();

}
add_shortcode("mmdigest", "mmdigest_shortcode");