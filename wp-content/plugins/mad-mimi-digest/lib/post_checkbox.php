<?php

/* =======================================================
/* ADD ACTIONS FOR POST EDITOR
/* ======================================================= */

add_action( 'load-post.php', 'mmdigest_post_meta_boxes_setup' );
add_action( 'load-post-new.php', 'mmdigest_post_meta_boxes_setup' );

/* =======================================================
/* MMDIGEST METABOX OPTIONS
/* ======================================================= */

function mmdigest_post_meta_boxes_setup() {
	add_action( 'add_meta_boxes', 'mmdigest_add_post_meta_boxes' );
	add_action( 'publish_post', 'mmdigest_save_post_checkbox_meta', 5, 2 );
	add_action( 'save_post', 'mmdigest_save_post_checkbox_meta', 4, 2 );
}

/* =======================================================
/* CREATE MMDIGEST METABOX
/* ======================================================= */

function mmdigest_add_post_meta_boxes() {
	add_meta_box(
		'mmdigest-post-checkbox',
		'Mad Mimi Digest options',
		'mmdigest_post_checkbox_meta_box',
		'post',
		'side',
		'default'
	);
}

/* =======================================================
/* RENDER MMDIGEST METABOX
/* ======================================================= */

function mmdigest_post_checkbox_meta_box( $object, $box ) { ?>
  <?php wp_nonce_field( basename( __FILE__ ), 'mmdigest_post_checkbox_nonce' ); ?>
  <p>
  	<input class="widefat" type="checkbox" name="mmdigest-post-checkbox" id="mmdigest-post-checkbox" 
	<?php
		$status = $object->post_status; // auto-draft
		$cur_checked = esc_attr(get_post_meta( $object->ID, 'mmdigest_post_checkbox_meta', true ));
		if ($status === 'auto-draft') {
			echo 'checked="checked"';
		} else if ($cur_checked === '') {
		} else if ($cur_checked === 'on') {
			echo 'checked="checked"';
		}
	?>
  	/>
    <label for="mmdigest-post-checkbox">Send in Mad Mimi Digest</label>
  </p>
<?php
}

/* =======================================================
/* SAVE MMDIGEST METABOX CHECKBOX
/* ======================================================= */

function mmdigest_save_post_checkbox_meta( $post_id, $post ) {

	if ( !isset( $_POST['mmdigest_post_checkbox_nonce'] ) || !wp_verify_nonce( $_POST['mmdigest_post_checkbox_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	$post_type = get_post_type_object( $post->post_type );

	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) ) {
		return $post_id;
	}

	$new_meta_value = ( isset( $_POST['mmdigest-post-checkbox'] ) ? $_POST['mmdigest-post-checkbox'] : '' );

	if ($_POST['original_post_status'] === 'auto-draft') {
		$new_meta_value = '';
	}

	$meta_key = 'mmdigest_post_checkbox_meta';

	$meta_value = get_post_meta( $post_id, $meta_key, true );

	if ( $new_meta_value && '' == $meta_value ) {
		add_post_meta( $post_id, $meta_key, $new_meta_value, true );
	}

	if ( $new_meta_value && $new_meta_value != $meta_value ) {
		update_post_meta( $post_id, $meta_key, $new_meta_value );
	}

	if ( '' == $new_meta_value && $meta_value ) {
		delete_post_meta( $post_id, $meta_key, $meta_value );
	}

}