<?php

/* =======================================================
/* GET PLUGIN CONFIGURATIONS
/* ======================================================= */

function mmdigest_getConfiguration()
{
	$config = stripslashes(get_option('mmd_config'));
	$config = json_decode($config);
	
	return $config;
}

/* =======================================================
/* GET MAD MIMI AUDIENCES
/* ======================================================= */

function mmdigest_getMadMimiAudiences($mm = null)
{
	$mm_audiences = '';
	$config = mmdigest_getConfiguration();

	if (!$mm) {
		$mm = new MadMimi($config->username, $config->api, false);
	}

	$mm_audiences = $mm->Lists();
	
	if ($mm_audiences != "Couldn't authenticate you") {
        $mm_audiences = new SimpleXMLElement($mm_audiences);
    } 
	
	return $mm_audiences;
}

/* =======================================================
/* GET MAD MIMI AUDIENCE NAME
/* ======================================================= */

function mmdigest_getMadMimiAudienceName($id, $mm = null)
{
	$audiences = mmdigest_getMadMimiAudiences($mm);

    if ($audiences){ 
        foreach ($audiences as $audience) { 
            if ($id == $audience['id']) {
                return $audience['name'];
            }
        }
    }
    return null;
}

/* =======================================================
/* GET MAD MIMI PROMOTIONS
/* ======================================================= */

function mmdigest_getMadMimiPromotions($mm = null)
{
	$mm_audiences = '';
	$config = mmdigest_getConfiguration();
	
	if ($config->username == '' || $config->api == '') {
		return false;
	}

	if (!$mm) {
		$mm = new MadMimi($config->username, $config->api, false);
	}
	
	$mm_promotions = $mm->Promotions();

	if ($mm_promotions !== 'Unable to authenticate' || $mm_promotions == '') {
        $mm_promotions = new SimpleXMLElement($mm_promotions);
        return $mm_promotions;
    } else {
    	return false;
    }

}

/* =======================================================
/* GET MAD MIMI PROMOTION NAME
/* ======================================================= */

function mmdigest_getMadMimiPromotionName($id, $mm = null)
{
	$promotions = mmdigest_getMadMimiPromotions($mm);
    
    if ($promotions){ 
        foreach ($promotions as $promotion) { 
            if ($id == $promotion['id']) {
                return $promotion['name'];
            }
        }
    }
    return null;
}

/* =======================================================
/* FUNCTION TO GET THE CLIENT IP ADDRESS
/* ======================================================= */

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

/* =======================================================
/* DELETE SINGLE SUBSCRIBE
/* ======================================================= */

function mmdigest_delete_single_subscribe($id) {
    
	global $wpdb;
	$wpdb->query( "DELETE FROM `{$wpdb->prefix}mmdigest` WHERE `id` = {$id}" );
}

/* =======================================================
/* DELETE SINGLE SUBSCRIBE AS EMAIL
/* ======================================================= */

function mmdigest_delete_single_subscribe_email($email) {
    
	global $wpdb;
	$wpdb->query("DELETE FROM `{$wpdb->prefix}mmdigest` WHERE (`email` = '{$email}')");

}

/* =======================================================
/* DELETE BULK SUBSCRIBES
/* ======================================================= */

function mmdigest_delete_bulk_subscribes($id_array) {
    
	global $wpdb;

	foreach ($id_array as $id) {
		$id = (int) $id;
		$wpdb->query("DELETE FROM `{$wpdb->prefix}mmdigest` WHERE `id` = {$id}");
	}
}

/* =======================================================
/* SUBSCRIBE USERS FROM TEXTAREA
/* ======================================================= */

function mmdigest_subscribe_textarea_users($emails) {
    
	$split_strings = preg_split("/[\r\n,]+/", $emails, -1, PREG_SPLIT_NO_EMPTY);

	foreach ($split_strings as $split) {

		if ($split != '') {

			$explode = explode("|", $split);

			if ($explode[0]) {
				$email = $explode[0];
			} else {
				$email = '';
			}

			if ($explode[1]) {
				$digest = $explode[1];
			} else {
				$digest = 'Instant';
			}

			if (preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email)) {
				mmdigest_addUserToWp($email, $digest);
			}

		}

	}

}

/* =======================================================
/* UNSUBSCRIBE USERS FROM TEXTAREA
/* ======================================================= */

function mmdigest_unsubscribe_textarea_users($emails) {
    
	$split_strings = preg_split("/[\r\n,]+/", $emails, -1, PREG_SPLIT_NO_EMPTY);

	foreach ($split_strings as $email) {
		//if (preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email)) {
			mmdigest_delete_single_subscribe_email($email);
		//}
	}
}