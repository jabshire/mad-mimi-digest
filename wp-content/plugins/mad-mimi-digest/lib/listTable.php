<?php

/* =======================================================
/* CLASS FOR SUBSCRIBERS LIST TABLE
/* ======================================================= */

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Subscribers_List_Table extends WP_List_Table {
 
    function __construct(){

	    global $status, $page, $wpdb;
	 
        $this->db = $wpdb;

	        parent::__construct( array(
	            'singular'  => 'subscriber',     //singular name of the listed records
	            'plural'    => 'subscribers',   //plural name of the listed records
	            'ajax'      => false     	   //does this table support ajax?
	    ) );
	 
    }

	function no_items() {
		_e( 'No subscribers found.' );
	}
	 
	function column_default( $item, $column_name ) {
		switch( $column_name ) { 
			case 'email':
			case 'first_name':
			case 'last_name':
			case 'subscription_schedule':
				return $item[ $column_name ];
			case 'datetime':
				return $item[ $column_name ];
			default:
				return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
		}
	}
	 
	function get_sortable_columns() {
		$sortable_columns = array(
			'email'			=> array('email',false),
			'first_name'	=> array('first_name',false),
			'last_name'		=> array('last_name',false),
			'subscription_schedule'		=> array('subscription_schedule',false),
			'datetime'		=> array('datetime',false)
		);
		return $sortable_columns;
	}
	 
	function get_columns(){
		$columns = array(
			'cb'			=> '<input type="checkbox" />',
			'email'			=> 'Email',
			'first_name'	=> 'First Name',
			'last_name'		=> 'Last Name',
			'subscription_schedule'		=> 'Digest',
			'datetime'		=> 'Date'
		);
		return $columns;
	}
	 
	function usort_reorder( $a, $b ) {
	  // If no sort, default to title
	  $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'email';
	  // If no order, default to asc
	  $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'asc';
	  // Determine sort order
	  $result = strcmp( $a[$orderby], $b[$orderby] );
	  // Send final sort direction to usort
	  return ( $order === 'asc' ) ? $result : -$result;
	}
	 
	function column_email($item){
	  $actions = array(
	           // 'edit'      => sprintf('<a href="?page=%s&action=%s&mmdigest=%s">Edit</a>',$_REQUEST['page'],'edit',$item['id']),
	            'delete'    => sprintf('<a href="?page=%s&action=%s&delete_id=%s">Delete</a>',$_REQUEST['page'],'delete',$item['id']),
	        );
	 
	  return sprintf('%1$s %2$s', $item['email'], $this->row_actions($actions) );
	}
	 
	function get_bulk_actions() {
	  $actions = array(
	    'delete'    => 'Delete'
	  );
	  return $actions;
	}
	 
	function column_cb($item) {
		return sprintf(
			'<input type="checkbox" name="subscribers[]" value="%s" />', $item['id']
		);    
	}
	 
	function prepare_items($search = NULL) {
	  $columns  = $this->get_columns();
	  $hidden   = array();
	  $sortable = $this->get_sortable_columns();
	  $this->_column_headers = array( $columns, $hidden, $sortable );

	  if( $search != NULL ){
		$search = trim($search);
		$subscribers_data = $this->db->get_results("SELECT id, first_name, last_name, email, subscription_schedule, DATE_FORMAT(FROM_UNIXTIME(create_date), '%d/%m/%Y %h:%i:%s') AS create_date, ip, active, DATE_FORMAT(FROM_UNIXTIME(datetime), '%d/%m/%Y %h:%i:%s') AS datetime, ip, active FROM ".$this->db->prefix."mmdigest WHERE `first_name` LIKE '%".$search."%' OR `last_name` LIKE '%".$search."%' OR `email` LIKE '%".$search."%' OR `subscription_schedule` LIKE '%".$search."%'");
	  } else {
		$subscribers_data = $this->db->get_results("SELECT id, first_name, last_name, email, subscription_schedule, DATE_FORMAT(FROM_UNIXTIME(create_date), '%d/%m/%Y %h:%i:%s') AS create_date, ip, active, DATE_FORMAT(FROM_UNIXTIME(datetime), '%d/%m/%Y %h:%i:%s') AS datetime, ip, active FROM ".$this->db->prefix."mmdigest");
	  }

	  $subscr_arr = array();
	  foreach ($subscribers_data as $key => $value) {
	  	$subscr_arr[] = (array)$value;
	  }

	   usort( $subscr_arr, array( &$this, 'usort_reorder' ) );
	  
	  $per_page = 25;
	  $current_page = $this->get_pagenum();
	  $total_items = count( $subscr_arr );
	 
	  // only ncessary because we have sample data
	  $this->found_data = array_slice( $subscr_arr,( ( $current_page-1 )* $per_page ), $per_page );
	 
	  $this->set_pagination_args( array(
	    'total_items' => $total_items, 
	    'per_page'    => $per_page
	  ) );
	  $this->items = $this->found_data;
	}
 
}