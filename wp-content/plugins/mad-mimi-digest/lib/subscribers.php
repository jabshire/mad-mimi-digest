<?php

/* =======================================================
/* Mad Mimi Digest > Subscribers
/* ======================================================= */

function mmdigest_subscribers_table() {

	// Actions for delete subscribers
	if (isset($_GET['page']) && isset($_GET['action']) && isset($_GET['delete_id'])) {
		if ($_GET['page'] == 'mmdigest_subscribers' && $_GET['action'] == 'delete') {
			$delete_id = (int) $_GET['delete_id'];
			mmdigest_delete_single_subscribe($delete_id);
		}
	}

	if (isset($_POST['action']) && isset($_POST['action2']) && isset($_POST['subscribers'])) {
		if ($_POST['action'] == 'delete' || $_POST['action2'] == 'delete') {
			mmdigest_delete_bulk_subscribes($_POST['subscribers']);
		}
	}

	// Actions for subscribe and unsubscribe users
	if ((isset($_POST['action_unsubscribe']) || isset($_POST['action_subscribe'])) && isset($_POST['all_textarea'])) {

		if (isset($_POST['action_subscribe']) && $_POST['action_subscribe'] == 'Subscribe') {
			mmdigest_subscribe_textarea_users($_POST['all_textarea']);
		}

		if (isset($_POST['action_unsubscribe']) && $_POST['action_unsubscribe'] == 'Unsubscribe') {
			mmdigest_unsubscribe_textarea_users($_POST['all_textarea']);
		}
	}

	$subscribers_list = new Subscribers_List_Table();

?>

<div class="wrap">
	<h3>Add/Remove Subscribers</h3>
	<p>
	<form method="post">
		Enter addresses, one per line or comma-separated<br>
		<textarea id="hhh" name="all_textarea" rows="5" cols="50"></textarea><br>
		<input type="submit" class="button button-primary" name="action_subscribe" value="Subscribe">
		<input type="submit" class="button button-primary" name="action_unsubscribe" value="Unsubscribe">
	</form>
	</p>
	<h3>Current Subscribers</h3>
	<form method="post">
		<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>">
		<?php
		if(isset($_POST['s'])){
			$subscribers_list->prepare_items($_POST['s']);
		} else {
			$subscribers_list->prepare_items();
		}
		$subscribers_list->search_box('search', 'search_id');
		$subscribers_list->display(); 
		?>
	</form>
</div>
<?
}