<?php

date_default_timezone_set('America/Chicago');

/* =======================================================
/* CRON CONTROL INTERVALS
/* ======================================================= */

function mmdigest_cron_intervals($schedules) {
	$schedules['mmdigest_instant'] = array(
		'interval' => 60,
		'display' => __('Instant')
	);
	$schedules['mmdigest_daily'] = array(
		'interval' => 86400,
		'display' => __('Once a day')
	);
	$schedules['mmdigest_weekly'] = array(
		'interval' => 604800,
		'display' => __('Once a week')
	);
	return $schedules;
}
add_filter( 'cron_schedules', 'mmdigest_cron_intervals'); 

/* =======================================================
/* CRON ACTIVATE SCHEDULE
/* ======================================================= */

add_action( 'mmdigest_instant_hook', 'mmdigest_instant_function' );
if ( ! wp_next_scheduled( 'mmdigest_instant_hook' ) ) {
  wp_schedule_event( time(), 'mmdigest_instant', 'mmdigest_instant_hook' );
}

add_action( 'mmdigest_daily_hook', 'mmdigest_daily_function' );
if ( ! wp_next_scheduled( 'mmdigest_daily_hook' ) ) {
  wp_schedule_event( time(), 'mmdigest_daily', 'mmdigest_daily_hook' );
}

add_action( 'mmdigest_weekly_hook', 'mmdigest_weekly_function' );
if ( ! wp_next_scheduled( 'mmdigest_weekly_hook' ) ) {
  wp_schedule_event( time(), 'mmdigest_weekly', 'mmdigest_weekly_hook' );
}

function mmdigest_instant_function() {
	// mmdigest_send_cron_emails('Instant');
}

function mmdigest_daily_function() {
	mmdigest_send_cron_emails('Daily');
}

function mmdigest_weekly_function() {
	mmdigest_send_cron_emails('Weekly');
}

/* =======================================================
/* CRON SEND EMAIL'S
/* ======================================================= */

function mmdigest_send_cron_emails($type) {

	global $wpdb;

	$mmd_config = (array) json_decode(wp_unslash(get_option('mmd_config')));
	
	$mm_username = $mmd_config['username'];
	$mm_api = $mmd_config['api'];
	$mm_from = $mmd_config['from'];
	$prom_instant_name = $mmd_config['promo_instant'];
	$prom_daily_name = $mmd_config['promo_daily'];
	$prom_weekly_name = $mmd_config['promo_weekly'];
	$mm_post_type = $mmd_config['post_type'];
	
	$madMimi = new MadMimi($mm_username, $mm_api);
	$mm_promotions = mmdigest_getMadMimiPromotions($madMimi);

	$digest_audience = 'Digest '.$type;

	if (($mmd_config['username'] == '' || $mmd_config['api'] == '' || $mm_promotions === 'Unable to authenticate')) {

		return false;

	} else {

		/* ======================================================= */
		// ADD SUBSCRIBERS TO DIGEST LIST
		// GET USERS FOR CURRENT DIGEST ONLY
		/* ======================================================= */
		
		$users = array();
		$subscribers = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}mmdigest");
		
		if ($subscribers):
			foreach ($subscribers as $subscriber):
				if ($subscriber->subscription_schedule === $type):
					
					$user = array(
						'email' => $subscriber->email,
						'firstName' => $subscriber->first_name,
						'lastName' => $subscriber->last_name,
						'add_list' => $digest_audience,
						);
					$users[] = $user;
					
				endif;
			endforeach;
		else:
			// return false;
		endif;

		/* ======================================================= */
		// GET POSTS FOR DIGEST: $posts
		/* ======================================================= */

		$end = time();
		$time_range = 0;
		
		if ($type === 'Daily'):
			$time_range = 86400;
		
		elseif ($type === 'Weekly'):
			$time_range = 604800;
		
		elseif ($type === 'Instant'):
			$time_range = 60;
		
		endif;
		
		$start = $end - $time_range;
		
		$start_frmtd = date('Y-m-d G:i:s', $start);
		$end_frmtd = date('Y-m-d G:i:s', $end);
		
		$posts = $wpdb->get_results("SELECT ID, post_title, post_content FROM wp_posts WHERE post_date >= '".$start_frmtd."' AND post_date<='".$end_frmtd."' AND post_status='publish'");

		/* ======================================================= */
		// SET PROMOTION NAME
		// which is set in welcome, config, or default
		/* ======================================================= */

		$prom_name = null;
		
		if ($type === 'Daily'):

			if ($mmd_config['promo_daily']) {
				$prom_name = (string) mmdigest_getMadMimiPromotionName($mmd_config['promo_daily'], $madMimi);
			} else {
				$prom_name = (string) $mm_promotions->promotion[0]['name'];
			}

		elseif ($type === 'Weekly'):

			if ($mmd_config['promo_weekly']) {
				$prom_name = (string) mmdigest_getMadMimiPromotionName($mmd_config['promo_weekly'], $madMimi);
			} else {
				$prom_name = (string) $mm_promotions->promotion[0]['name'];
			}

		elseif ($type === 'Instant'):
			
			if ($mmd_config['promo_instant']) {
				$prom_name = (string) mmdigest_getMadMimiPromotionName($mmd_config['promo_instant'], $madMimi);
			} else {
				$prom_name = (string) $mm_promotions->promotion[0]['name'];
			}

		endif;

		/* ======================================================= */
		// SEND POSTS TO SUBSCRIBERS
		/* ======================================================= */

		if ($posts && $prom_name) {
			
			$digest = '';
			
			foreach($posts as $post) { // compiles {body}
				
				$post_id = $post->ID;
				$post_title = $post->post_title;

				$post_content = $post->post_content; 
				$post_content = apply_filters('the_content', $post_content); 
				$post_content = preg_replace('/<img[^>]+./','', $post_content);
				$post_link = get_permalink($post_id);

				// Check meta box value to see if digest requested
				
				$mmdigest_post_checkbox = get_post_meta($post_id, 'mmdigest_post_checkbox_meta', true);
				if ($mmdigest_post_checkbox == '') {
					continue;
				}

				switch ($mm_post_type) {

					case 'full_post' :
						$more_link = '<p><a href="' . $post_link . '" style="color:#a2885b;"><strong>Read this online.</strong></a></p>';
					break;		

					case 'excerpt_post' :
						$post_content = strip_tags($post_content);
						$post_excerpt = mmdigest_get_the_post_excerpt($post_id);

						if ($post_excerpt == '' || empty($post_excerpt)) {

							if (strlen($post_content) > 200) {
								$post_content = substr($post_content, 0, 200);
								$post_content = rtrim($post_content, "!,.-");
								$post_content = substr($post_content, 0, strrpos($post_content, ' '));
								$post_content .= '...';
							}

						} else {
							$post_content = '';
							$post_content .= wpautop($post_excerpt);
						}

						$more_link = '<p><a href="' . $post_link . '" style="color:#a2885b;"><strong>Read more.</strong></a></p>';
					break;

					case 'title_post' :
						$post_content = '';
						$more_link = '';
					break;

					default:
						$more_link = '<p><a href="' . $post_link . '" style="color:#a2885b;"><strong>Read this online.</strong></a></p>';
					break;
				}

				$digest .= '<h1><a href="' . $post_link . '" style="font-family:helvetica,arial,sans-serif;font-size:24px;font-weight:900;line-height:30px;color:#323232;text-decoration:none;border:none;">' . $post_title . '</a></h1>';
				$digest .= $post_content;
				$digest .= $more_link;

			}

			// sync users
			if (!empty($users)) {
				$csv_file = "email,first_name,last_name,add_list";
				foreach ($users as $user) {
					$csv_file .= "\n" . $user['email'] . "," . $user['firstName'] . "," . $user['lastName'] . "," . $user['add_list'];
				}
				$madMimi->Import($csv_file, false);
			}

			//error_log($digest_audience, 0);
			
			$options = array(
				'promotion_name'	=> $prom_name,
				'list_name'			=> $digest_audience,
				'subject'			=> $post_title,
				'from'				=> $mm_from,
			);

			$body = array('body' => $digest);

			$madMimi->SendMessage($options, $body, false);

		} else {
			return false;
		}

		return true;
	}
}

/* =======================================================
/* REMOVE IMAGES FROM CONTENT
/* ======================================================= */

function remove_images($content){
   $content = preg_replace('/<img[^>]+./','', $content);
   return $content;
}

/* =======================================================
/* SEND EMAIL FOR POST PUBLISH
/* ======================================================= */

function send_email_post_publish($postID) {
	
	global $wpdb;

	$mmd_config = (array) json_decode(wp_unslash(get_option('mmd_config')));

	$mm_username = $mmd_config['username'];
	$mm_api = $mmd_config['api'];
	$mm_from = $mmd_config['from'];
	$mm_post_type = $mmd_config['post_type'];

	$madMimi = new MadMimi($mm_username, $mm_api);
	$prom_name = (string) mmdigest_getMadMimiPromotionName($mmd_config['promo_instant'], $madMimi);
	$mm_promotions = mmdigest_getMadMimiPromotions($madMimi);

	if (($mmd_config['username'] == '' || $mmd_config['api'] == '' || $mm_promotions === 'Unable to authenticate')) {

		return false;

	} else {

		$users = array();
		$subscribers = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}mmdigest");
		$type = 'Instant'; // 'Weekly';
		$digest_audience = 'Digest ' . $type;

		// Get all subscribers
		if ($subscribers):
			foreach ($subscribers as $subscriber):
				if ($subscriber->subscription_schedule === $type):
					
					$user = array(
						'email' => $subscriber->email,
						'firstName' => $subscriber->first_name,
						'lastName' => $subscriber->last_name,
						'add_list' => 'Digest Instant',
						);
					$users[] = $user;
					
				endif;
			endforeach;
		else:
			// return false;
		endif;

		// Get post by ID
		$posts = $wpdb->get_results("SELECT ID, post_title, post_content FROM wp_posts WHERE ID=" . $postID);

		if ($posts) {
			
			$digest = '';
			
			foreach($posts as $post) { // compiles {body}
				
				$post_id = $post->ID;
				$post_title = $post->post_title;

				$post_content = $post->post_content; 
				$post_content = apply_filters('the_content', $post_content); 
				$post_content = preg_replace('/<img[^>]+./','', $post_content);
				$post_link = get_permalink($post_id);

				// Check meta box value
				$mmdigest_post_checkbox = get_post_meta($post_id, 'mmdigest_post_checkbox_meta', true);
				if ($mmdigest_post_checkbox == '') {
					continue;
				}

				switch ($mm_post_type) {

					case 'full_post' :
						$more_link = '<p><a href="' . $post_link . '" style="color:#a2885b;"><strong>Read this online.</strong></a></p>';
					break;		

					case 'excerpt_post' :

						$post_content = strip_tags($post_content);
						$post_excerpt = mmdigest_get_the_post_excerpt($post_id);

						if ($post_excerpt == '' || empty($post_excerpt)) {

							if (strlen($post_content) > 200) {
								$post_content = substr($post_content, 0, 200);
								$post_content = rtrim($post_content, "!,.-");
								$post_content = substr($post_content, 0, strrpos($post_content, ' '));
								$post_content .= '...';
							}
							
						} else {
							$post_content = '';
							$post_content .= wpautop($post_excerpt);
						}

						$more_link = '<p><a href="' . $post_link . '" style="color:#a2885b;"><strong>Read more.</strong></a></p>';
					break;

					case 'title_post' :
						$post_content = '';
						$more_link = '';
					break;

					default:
						$more_link = '<p><a href="' . $post_link . '" style="color:#a2885b;"><strong>Read this online.</strong></a></p>';
					break;
				}

				$digest .= '<h1><a href="' . $post_link . '" style="font-family:helvetica,arial,sans-serif;font-size:24px;font-weight:900;line-height:30px;color:#323232;text-decoration:none;border:none;">' . $post_title . '</a></h1>';
				$digest .= $post_content;
				$digest .= $more_link;

			}
		} else {
			return false;
		}

		// sync users
		if (!empty($users)) {
			$csv_file = "email,first_name,last_name,add_list";
			foreach ($users as $user) {
				$csv_file .= "\n" . $user['email'] . "," . $user['firstName'] . "," . $user['lastName'] . "," . $user['add_list'];
			}
			$madMimi->Import($csv_file, false);
		}

		$options = array(
			'promotion_name'	=> $prom_name,
			'list_name'			=> 'Digest Instant',
			'subject'			=> $post_title,
			'from'				=> $mm_from,
		);

		$body = array('body' => $digest);

		// Send email's
		$madMimi->SendMessage($options, $body, false);
	}
}

/* =======================================================
/* SEND EMAIL FOR INSTANT AUDIENCE
/* ======================================================= */

function post_publish_email_send($post_ID) {

	if ($_POST) {

		if(($_POST['post_status'] == 'publish') && ($_POST['original_post_status'] != 'publish')) {
			
			// Send new 'Instant' email
			send_email_post_publish($post_ID);

			// Update meta data for mmdigest checkbox
			$new_status = '';
			update_post_meta( $post_ID, 'mmdigest_post_checkbox_meta', esc_attr($new_status) );

		}
	} else {
		send_email_post_publish($post_ID);
	}
}

add_action('publish_post', 'post_publish_email_send', 30, 1);
// add_action('publish_future_post', 'post_publish_email_send');

/* =======================================================
/* GET SINGLE EXCERPT BY POST ID
/* ======================================================= */

function mmdigest_get_the_post_excerpt($post_id) {
	global $post;  
	$save_post = $post;
	$post = get_post($post_id);
	$output = get_the_excerpt();
	$post = $save_post;
	return $output;
}

/* =======================================================
/* CRON DEBUG
/* ======================================================= */

// For instant (60 sec interval)
// var_dump(gmdate("Y-m-d H:i:s", wp_next_scheduled( 'mmdigest_instant_hook' )));

// For daily (86400 sec interval)
// var_dump(gmdate("Y-m-d H:i:s", wp_next_scheduled( 'mmdigest_daily_hook' )));

// For weekly (604800 sec interval)
// var_dump(gmdate("Y-m-d H:i:s", wp_next_scheduled( 'mmdigest_weekly_hook' )));