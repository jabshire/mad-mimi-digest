<?php
/*
 * Plugin Name: Mad Mimi Digest
 * Plugin URI: www.jacobabshire.com/projects/mad-mimi-digest/
 * Description: Integrates Mad Mimi to allow subscriptions and digest emails to be sent in Mad Mimi Promotions.
 * Version: 1.0
 * Author: Jacob Abshire
 * Author URI: www.jacobabshire.com
 * License: GPL2
*/

/*
 * Include Files
 */

require(dirname(__FILE__) . '/lib/MadMimi.class.php');
require(dirname(__FILE__) . '/lib/cronControl.php');

include_once(plugin_dir_path(__FILE__).'mad-mimi-subscribe-widget.php');

/*
 * Initialize
 */
function wp_madmimi_activation() {}
register_activation_hook(__FILE__, 'wp_madmimi_activation');

function wp_madmimi_deactivation() {}
register_deactivation_hook(__FILE__, 'wp_madmimi_deactivation');

add_action('admin_menu', 'wp_madmimi');
function wp_madmimi() {
    add_menu_page('Mad Mimi Digest', 'Mad Mimi Digest', 'administrator', 'mmd_settings', 'mmd_display');
}

/*
 * Request handler
 */
if(isset($_POST['mm_username']) && isset($_POST['mm_api'])) {
    $mm_username = null;
    $mm_api = null;
    if($_POST['mm_username']) {
        $mm_username = $_POST['mm_username'];
    }

    if($_POST['mm_api']) {
        $mm_api = $_POST['mm_api'];
    }
    if($mm_username && $mm_api) {
        global $wpdb;
        
        // username
        $madmimi_username = $wpdb->get_row("SELECT option_id, option_name, option_value FROM wp_options WHERE option_name='madmimi_username'");
        if($madmimi_username) {
            $wpdb->query("UPDATE wp_options SET option_value='".$mm_username."' WHERE option_id='".$madmimi_username->option_id."'");
        } else {
            $wpdb->query("INSERT INTO wp_options (option_id, option_name, option_value, autoload) VALUES('', 'madmimi_username', '".$mm_username."', 'yes')");
        }
        
        // api
        $madmimi_api = $wpdb->get_row("SELECT option_id, option_name, option_value FROM wp_options WHERE option_name='madmimi_api'");
        if($madmimi_api) {
            $wpdb->query("UPDATE wp_options SET option_value='".$mm_api."' WHERE option_id='".$madmimi_api->option_id."'");
        } else {
            $wpdb->query("INSERT INTO wp_options (option_id, option_name, option_value, autoload) VALUES('', 'madmimi_api', '".$mm_api."', 'yes')");
        }
    }
    
    header("Location: ", TRUE, "302");
}

/*
 * Subscribe user
 */
if(isset($_POST['subscribe'])) {
    $subscribe = $_POST['subscribe'];
    
    $email = $subscribe['email'];
    $fname = $subscribe['fname'];
    $lname = $subscribe['lname'];
    
    $selected_aud_list = getAudienceListSelected();

    if($selected_aud_list && $email) {
        $selected_aud_llst_name = getNameAudienceList($selected_aud_list);
        
        $user = array('email' => $email, 'firstName' => $fname, 'lastName' => $lname, 'add_list' => $selected_aud_llst_name); 

        $arr_mm_config = getMadmimiConfiguration();
        $mm_username = $arr_mm_config['mm_username'];
        $mm_api = $arr_mm_config['mm_api'];

        $mimi = new MadMimi($mm_username, $mm_api, false);
        $mimi->AddUser($user);
        
        // run widget
        startWidget(true);
    }
    addUserSubscribe($email, $fname, $lname);
} else {
    // run widget
    startWidget();
}

/*
 * Handler to notify
 */
$schedule_type = getScheduleSelected();
if($schedule_type == 1) { // Send email once post is published
    add_action('publish_post', 'notifyNewPost');
} else if($schedule_type == 2) { // Send email every hour
    $wpTC = new WP_TestCron();
    $wpTC->cron_activate('every_hour');
} else if($schedule_type == 3) { // Send email to times per day
    $wpTC = new WP_TestCron();
    $wpTC->cron_activate('two_times_per_day');
} else if($schedule_type == 4) { // Send email to mail every day
    $wpTC = new WP_TestCron();
    $wpTC->cron_activate('every_day');
}
   
/*
 * Notify new post
 */
function notifyNewPost($post_id) {
    if( ( $_POST['post_status'] == 'publish' ) && ( $_POST['original_post_status'] != 'publish' ) ) {
        
        $post = get_post($post_id); 
        $title = $post->post_title;

        $email_contect = 'New post <a href="'.get_site_url().'/?p='.$post_id.'">'.$title.'</a>';
        
        $arr_mm_config = getMadmimiConfiguration();
        $mm_username = $arr_mm_config['mm_username'];
        $mm_api = $arr_mm_config['mm_api'];
        
        $prom_name = getPromotionName();
        
        $selected_aud_list = getAudienceListSelected();
        $selected_aud_llst_name = getNameAudienceList($selected_aud_list);
        
        if($prom_name && $selected_aud_llst_name) {
            $madMimi = new MadMimi($mm_username, $mm_api);
            $options = array(   'list_name' => (string)$selected_aud_llst_name,
                                'promotion_name' => (string)$prom_name,
                                'subject' => $title,
                                'from' => 'help@wt-test.com');
            $body = array('body' => $email_contect);
            $madMimi->SendMessage($options, $body);
        }
    }
}


/*
 * Set audience list
 */
if(isset($_POST['aud_list'])) {
    $select_al = $_POST['aud_list'][0];
    global $wpdb;
    
    $madmimi_aud_list = $wpdb->get_row("SELECT option_id, option_name, option_value FROM wp_options WHERE option_name='madmimi_aud_list'");
    if($madmimi_aud_list) {
        $wpdb->query("UPDATE wp_options SET option_value='".$select_al."' WHERE option_id='".$madmimi_aud_list->option_id."'");
    } else {
        $wpdb->query("INSERT INTO wp_options (option_id, option_name, option_value, autoload) VALUES('', 'madmimi_aud_list', '".$select_al."', 'yes')");
    }
}

/*
 * Set promotion list
 */
if(isset($_POST['prom_list'])) {
    $select_pl = $_POST['prom_list'][0];
    global $wpdb;
    
    $madmimi_prom_list = $wpdb->get_row("SELECT option_id, option_name, option_value FROM wp_options WHERE option_name='madmimi_prom_list'");
    if($madmimi_prom_list) {
        $wpdb->query("UPDATE wp_options SET option_value='".$select_pl."' WHERE option_id='".$madmimi_prom_list->option_id."'");
    } else {
        $wpdb->query("INSERT INTO wp_options (option_id, option_name, option_value, autoload) VALUES('', 'madmimi_prom_list', '".$select_pl."', 'yes')");
    }
}

/*
 * Set schedule list
 */
if(isset($_POST['schedule_list'])) {
    $select_sl = $_POST['schedule_list'][0];
    global $wpdb;
    
    $schedule_list = $wpdb->get_row("SELECT option_id, option_name, option_value FROM wp_options WHERE option_name='schedule_list'");
    if($schedule_list) {
        $wpdb->query("UPDATE wp_options SET option_value='".$select_sl."' WHERE option_id='".$schedule_list->option_id."'");
    } else {
        $wpdb->query("INSERT INTO wp_options (option_id, option_name, option_value, autoload) VALUES('', 'schedule_list', '".$select_sl."', 'yes')");
    }
}

/*
 * Add user to subscribe
 */
function addUserSubscribe($email, $fname, $lname) {
    global $wpdb;
    
    // Create table wp_user_subscribe if not exist
    $table_sql = "CREATE TABLE IF NOT EXISTS `wp_user_subscribe` (
                        `id` int(11) unsigned NOT NULL auto_increment,
                        `email` varchar(255) NOT NULL default '',
                        `first_name` varchar(255) NOT NULL default '',
                        `last_name` varchar(500) NOT NULL default '',
                        PRIMARY KEY  (`id`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;";
    $wpdb->query($table_sql);
    
    $user_select = $wpdb->get_row("SELECT id FROM wp_user_subscribe WHERE email='".$email."'");
    if(!$user_select) {
        $wpdb->query("INSERT INTO wp_user_subscribe (id, email, first_name, last_name) VALUES('', '".$email."', '".$fname."', '".$lname."')");
    }
}

/*
 * Get selected value from audience list
 */
function getAudienceListSelected() {
    global $wpdb;
    
    $madmimi_aud_list = $wpdb->get_row("SELECT option_id, option_name, option_value FROM wp_options WHERE option_name='madmimi_aud_list'");
    if($madmimi_aud_list) {
        return $madmimi_aud_list->option_value;
    }
    return null;
}

/*
 * Get selected value from promotion list
 */
function getPromotionListSelected() {
    global $wpdb;
    
    $madmimi_aud_list = $wpdb->get_row("SELECT option_id, option_name, option_value FROM wp_options WHERE option_name='madmimi_prom_list'");
    if($madmimi_aud_list) {
        return $madmimi_aud_list->option_value;
    }
    return null;
}

/*
 * Get promotion name
 */
function getPromotionName() {
    $id = getPromotionListSelected();
    $prom_list = getPromotionsList();
    
    if($prom_list) { 
        for($i = 0; $i < count($prom_list); $i++) {
            if($id == $prom_list->promotion[$i]['id']) {
                return $prom_list->promotion[$i]['name'];
            }
        }
    }
    return null;
}

/*
 * Get schedule selected
 */
function getScheduleSelected() {
    global $wpdb;

    $schedule_list = $wpdb->get_row("SELECT option_id, option_name, option_value FROM wp_options WHERE option_name='schedule_list'");
    if($schedule_list) {
        return $schedule_list->option_value;
    }
    return null;
}

/*
 * Get name of audience list
 */
function getNameAudienceList($code) {
    $aud_list = getAudienceList();
    if($aud_list) { 
        foreach ($aud_list as $list) { 
            if($code == $list['id']) {
                return $list['name'];
            }
        }
    }
    return null;
}

/*
 * Get all audience list from madmimi
 */
function getAudienceList() {
    $aud_list = null;
    
    $arr_mm_config = getMadmimiConfiguration();
    $mm_username = $arr_mm_config['mm_username'];
    $mm_api = $arr_mm_config['mm_api'];
    
    $mimi = new MadMimi($mm_username, $mm_api, false);
    $lists = $mimi->Lists();
    if($lists != "Couldn't authenticate you") {
        $aud_list = new SimpleXMLElement($lists);
    }   
    
    return $aud_list;
}

/*
 * Get madmimi configuration
 */
function getMadmimiConfiguration() {
    global $wpdb;
    $mm_username = null;
    $mm_api = null;
    
    // username
    $madmimi_username = $wpdb->get_row("SELECT option_id, option_name, option_value FROM wp_options WHERE option_name='madmimi_username'");
    if($madmimi_username) {
        $mm_username = $madmimi_username->option_value;
    }
        
    // api
    $madmimi_api = $wpdb->get_row("SELECT option_id, option_name, option_value FROM wp_options WHERE option_name='madmimi_api'");
    if($madmimi_api) {
        $mm_api = $madmimi_api->option_value;
    }
    
    $arr_ret['mm_username'] = $mm_username;
    $arr_ret['mm_api'] = $mm_api;
    
    return $arr_ret;
}

/*
 * Get madmimi promotions
 */
function getPromotionsList() {
    $prom_list = null;
    
    $arr_mm_config = getMadmimiConfiguration();
    $mm_username = $arr_mm_config['mm_username'];
    $mm_api = $arr_mm_config['mm_api'];
    
    $mimi = new MadMimi($mm_username, $mm_api, false);
    $lists = $mimi->Promotions();
    
    if($lists != "Couldn't authenticate you") {
        $prom_list = new SimpleXMLElement($lists);
    }
    
    return $prom_list;
}

/*
 * Display madmimi
 */
function mmd_display() {
    $arr_mm_config = getMadmimiConfiguration();
    $aud_list = getAudienceList();
    $prom_list = getPromotionsList();
    $selected_aud_list = getAudienceListSelected();
    $selected_prom_list = getPromotionListSelected();
    $schedule_selected = getScheduleSelected();
    $schedule_list = $schedule_selected;
    ?>
    <div class="wrap">
    	
    	<h2>Mad Mimi Digest Settings</h2>
		
		<!--tabs-->
		<?php  
		$active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'display_options'; 
		?>  
		<h2 class="nav-tab-wrapper">
			<a href="?page=mmd_settings&tab=integration" class="nav-tab <?php echo $active_tab == 'integration' ? 'nav-tab-active' : ''; ?>">Integration</a>
			<a href="?page=mmd_settings&tab=audiences" class="nav-tab <?php echo $active_tab == 'audiences' ? 'nav-tab-active' : ''; ?>">Audiences</a>
			<a href="?page=mmd_settings&tab=promotions" class="nav-tab <?php echo $active_tab == 'promotions' ? 'nav-tab-active' : ''; ?>">Promotions</a>
		</h2>
		<!--/tabs-->
		
		<!--tab-pages-->
		<form method="post" action="options.php">
			<?php 
			
			if ($active_tab == 'integration'){
				settings_fields( 'resurge_theme_display_options' );
				do_settings_sections( 'resurge_theme_display_options' );
			} elseif ($active_tab == 'audiences'){
				settings_fields( 'resurge_theme_social_options' );
				do_settings_sections( 'resurge_theme_social_options' );
			} elseif ($active_tab == 'promotions'){
				settings_fields( 'resurge_theme_social_options' );
				do_settings_sections( 'resurge_theme_social_options' );
			}
			
			submit_button(); 
			?>
		</form>
		<!--/tab-pages-->
		
		
		
		
		
	    <form action="" method="post">
	        <h3>Integration Settings</h3>
            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="mad_mimi_username" style="font-weight:bold;">Mad Mimi Username:</label>
                        </th>
                        <td>
                            <input name="mm_username" id="mad_mimi_username" size="40" type="text" value="<?php echo $arr_mm_config['mm_username']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="mad_mimi_api" style="font-weight:bold;">API Key:</label>
                        </th>
                        <td>
                            <input name="mm_api" id="mad_mimi_api" size="40" type="text" value="<?php echo $arr_mm_config['mm_api']; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
	        
	        <p class="submit">
	            <input class="button button-primary" name="save" value="Save Changes" type="submit">
	        </p>
	    </form>
                    
                    
        <?php if($arr_mm_config['mm_api'] && $arr_mm_config['mm_username']) { ?>
		<form action="" method="POST">
                <div class="postbox-container" style="width:65%;">
                    <div class="metabox-holder">
                        <div class="meta-box-sortables">
                            <div id="madmimisettings" class="postbox">
                                <h3 class="hndle"><span>Audience lists</span></h3>
                                <div class="inside">
                                    <table>
                                        <?php if($aud_list) { foreach ($aud_list as $list) { ?>       
                                            <tr>
                                                <td>
                                                    <?php
                                                        $chec_al = '';
                                                        if($selected_aud_list) {
                                                            if($selected_aud_list == $list['id']) {
                                                                $chec_al = 'checked="checked"';
                                                            }
                                                        } else {
                                                            $chec_al = '';//'checked="checked"';
                                                        }
                                                    ?>
                                                    <input type="radio" name="aud_list[]" value="<?php echo $list['id'];?>" <?php echo $chec_al; ?>>
                                                </td>
                                                <td>
                                                    <?php echo $list['name']; ?>
                                                </td>
                                            </tr>
                                        <?php } } ?>
                                    </table>
                                </div>
                            </div>
                            <p class="submit">
                                <input class="button-primary" name="save" value="Select audience list" type="submit">
                            </p>
                        </div>
                    </div>
                </div>
            </form>

            <form action="" method="POST">
                <div class="postbox-container" style="width:65%;">
                    <div class="metabox-holder">
                        <div class="meta-box-sortables">
                            <div id="madmimisettings" class="postbox">
                                <h3 class="hndle"><span>Promotion lists</span></h3>
                                <div class="inside">
                                    <table>
                                        <?php if($prom_list) { for($i = 0; $i < count($prom_list); $i++) { ?>       
                                            <tr>
                                                <td>
                                                    <?php
                                                        $chec_al = '';
                                                        if($selected_prom_list) {
                                                            if($selected_prom_list == $prom_list->promotion[$i]['id']) {
                                                                $chec_al = 'checked="checked"';
                                                            }
                                                        } else {
                                                            $chec_al = '';//checked="checked"';
                                                        }
                                                    ?>
                                                    <input type="radio" name="prom_list[]" value="<?php echo $prom_list->promotion[$i]['id'];?>" <?php echo $chec_al; ?>>
                                                </td>
                                                <td>
                                                    <?php echo $prom_list->promotion[$i]['name']; ?>
                                                </td>
                                            </tr>
                                        <?php } } ?>
                                    </table>
                                </div>
                            </div>
                            <p class="submit">
                                <input class="button-primary" name="save" value="Select promotion list" type="submit">
                            </p>
                        </div>
                    </div>
                </div>
            </form>

            <form action="" method="POST">
                <div class="postbox-container" style="width:65%;">
                    <div class="metabox-holder">
                        <div class="meta-box-sortables">
                            <div id="madmimisettings" class="postbox">
                                <h3 class="hndle"><span>Sending schedule</span></h3>
                                <div class="inside">
                                    <table>
                                        <tr>
                                            <td><input type="radio" name="schedule_list[]" value="1" <?php if($schedule_list == 1) { echo 'checked="checked"'; } ?>></td>
                                            <td>Once the post is created</td>
                                        </tr>
                                        <tr>
                                            <td><input type="radio" name="schedule_list[]" value="2" <?php if($schedule_list == 2) { echo 'checked="checked"'; } ?>></td>
                                            <td>Every hour</td>
                                        </tr>
                                        <tr>
                                            <td><input type="radio" name="schedule_list[]" value="3" <?php if($schedule_list == 3) { echo 'checked="checked"'; } ?>></td>
                                            <td>Two times per day</td>
                                        </tr>
                                        <tr>
                                            <td><input type="radio" name="schedule_list[]" value="4" <?php if($schedule_list == 4) { echo 'checked="checked"'; } ?>></td>
                                            <td>Every day</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <p class="submit">
                                <input class="button-primary" name="save" value="Select sending schedule" type="submit">
                            </p>
                        </div>
                    </div>
                </div>
            </form>
        <?php } ?>
    </div>
    <?php
}

?>