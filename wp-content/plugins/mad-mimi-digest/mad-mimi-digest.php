<?php
/*
 * Plugin Name: Mad Mimi Digest
 * Plugin URI: www.jacobabshire.com/projects/mad-mimi-digest/
 * Description: Integrates Mad Mimi to allow subscriptions and digest emails to be sent in Mad Mimi Promotions. (Deactivating plugin will delete your configuration settings, but not your subscriber lists in Wordpress or their accounts in your Mad Mimi audience.
 * Version: 1.0
 * Author: Jacob Abshire
 * Author URI: www.jacobabshire.com
 * License: GPL2
*/

/* =======================================================
/* INCLUDE FILES
/* ======================================================= */

include_once(plugin_dir_path(__FILE__).'/lib/MadMimi.class.php');
include_once(plugin_dir_path(__FILE__).'/lib/ExportImport.class.php');
include_once(plugin_dir_path(__FILE__).'/lib/get.php');
include_once(plugin_dir_path(__FILE__).'/lib/shortcode.php');
include_once(plugin_dir_path(__FILE__).'/lib/listTable.php');
include_once(plugin_dir_path(__FILE__).'/lib/cronControl.php');
include_once(plugin_dir_path(__FILE__).'/lib/config.php');
include_once(plugin_dir_path(__FILE__).'/lib/subscribers.php');
include_once(plugin_dir_path(__FILE__).'/lib/csv.php');
include_once(plugin_dir_path(__FILE__).'/lib/save.php');
include_once(plugin_dir_path(__FILE__).'/lib/post_checkbox.php');
include_once(plugin_dir_path(__FILE__).'mad-mimi-subscribe-widget.php');

/* =======================================================
/* ACTIVATE
/* ======================================================= */

function mmdigest_activation()
{
	global $wpdb;

	$table_name = $wpdb->prefix . "mmdigest";
	$charset_collate = '';

	if ( ! empty( $wpdb->charset ) ) {
	  $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
	}

	if ( ! empty( $wpdb->collate ) ) {
	  $charset_collate .= " COLLATE {$wpdb->collate}";
	}

	$sql = "CREATE TABLE IF NOT EXISTS {$table_name} (
		    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		    `first_name` varchar(256) NOT NULL,
		    `last_name` varchar(256) NOT NULL,
			`email` varchar(255) CHARACTER SET latin1 NOT NULL,
			`subscription_schedule` enum('Daily', 'Weekly', 'Instant') DEFAULT 'Instant',
			`create_date` int(10) unsigned NOT NULL DEFAULT '0',
			`datetime` int(10) unsigned NOT NULL DEFAULT '0',
			`ip` varchar(30) NOT NULL,
			`active` int(1) DEFAULT 1 NOT NULL,
		  PRIMARY KEY (`id`)
		) {$charset_collate};";
	$wpdb->query($sql);

}
register_activation_hook(__FILE__, 'mmdigest_activation');

/* =======================================================
/* DEACTIVATE
/* ======================================================= */

function mmdigest_deactivation()
{
	delete_option('mmd_config');

	// Delete table mmdigest

	global $wpdb;
	$table_name = $wpdb->prefix . "mmdigest";
	$sql = "DROP TABLE IF EXISTS {$table_name}";
	$wpdb->query($sql);

	// Deactivate cron
	wp_clear_scheduled_hook('mmdigest_instant_hook');
	wp_clear_scheduled_hook('mmdigest_daily_hook');
	wp_clear_scheduled_hook('mmdigest_weekly_hook');

}
register_deactivation_hook(__FILE__, 'mmdigest_deactivation');

/* =======================================================
/* UNINSTALL
/* ======================================================= */

function mmdigest_uninstall()
{
	if (!current_user_can('activate_plugins')) return;
	
	check_admin_referer('bulk-plugins');
	
	if (__FILE__ != WP_UNINSTALL_PLUGIN) return;
	
	delete_option('mmd_config');
	
}
register_uninstall_hook(__FILE__, 'mmdigest_uninstall' );

/* =======================================================
/* INITIALIZE
/* ======================================================= */

add_action('admin_menu', 'mmdigest');

function mmdigest()
{

    // Add Mad Mimi Digest main menu to admin sidebar
	add_menu_page(
		'Mad Mimi Digest', 
		'Mad Mimi Digest', 
		'manage_options', 
		'mmdigest', 
		'mmdigest_settings', 
		'dashicons-rss'
	);

	// Add Mad Mimi Digest Subscribers submenu
	add_submenu_page( 
		'mmdigest', 
		'Mad Mimi Digest Subscribers', 
		'Subscribers', 
		'manage_options', 
		'mmdigest_subscribers', 
		'mmdigest_subscribers'
	);

	// Add Mad Mimi Digest Settings submenu
	add_submenu_page( 
		'mmdigest', 
		'Mad Mimi Digest Settings', 
		'Settings', 
		'manage_options', 
		'mmdigest_settings', 
		'mmdigest_settings'
	);

	// Add Mad Mimi Digest CSV export/import
	add_submenu_page( 
		'mmdigest', 
		'Mad Mimi CSV export/import', 
		'CSV export/import', 
		'manage_options', 
		'mmdigest_csv_import_export', 
		'mmdigest_csv_import_export'
	);

	// Remove submenu duplicate
	remove_submenu_page('mmdigest', 'mmdigest');

}

/* =======================================================
/* INITIALIZE > Add "Settings" to Plugin Action Links
/* ======================================================= */

add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'mmdigest_action_links' );

function mmdigest_action_links($links)
{
   $settings = '<a href="'. get_admin_url(null, 'admin.php?page=mmdigest_settings') .'">Settings</a>';
   array_unshift($links, $settings);
   return $links;
}

/* =======================================================
/* SUBSCRIBERS PAGE
/* ======================================================= */

function mmdigest_subscribers()
{
	?>
	<div class="wrap">
		
		<h2>Mad Mimi Digest Subscribers</h2>
		
		<!--tab-pages-->
		<?php mmdigest_subscribers_table(); ?>
		<!--/tab-pages-->
		
	</div>
    <?php
}

/* =======================================================
/* SETTINGS PAGE
/* ======================================================= */

function mmdigest_settings()
{
	$mmd_config = mmdigest_getConfiguration();
    ?>
    <div class="wrap">
    	
    	<h2>Mad Mimi Digest Settings</h2>
		
		<!--tab-pages-->
		<form method="post" action="">
			<?php mmdigest_configuration($mmd_config); ?>
			<?php submit_button(); ?>
		</form>
		<!--/tab-pages-->
		
    </div>
    <?php
}

/* =======================================================
/* SUBSCRIBERS EXPORT / IMPORT CSV
/* ======================================================= */

function mmdigest_csv_import_export()
{
	?>
	<div class="wrap">
		
		<h2>Mad Mimi Digest Export/Import Subscribers</h2>
		
		<!--tab-pages-->
		<form method="post" action="">
			<?php mmdigest_csv_table(); ?>
		</form>
		<!--/tab-pages-->
		
	</div>
    <?php
}
