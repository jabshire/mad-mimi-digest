<?php

class mmd_widget extends WP_Widget {

	function __construct() {
		
		parent::__construct(
			'mmd_widget', 
			__('Mad Mimi Digest', 'mmd_widget_domain'), 
			array( 'description' => __( 'Mad Mimi Digest subscription form.', 'mmd_widget_domain' ), ) 
			);
	}

	public function widget($args, $instance) {

		$title = apply_filters( 'widget_title', $instance['title'] );
		$description = $instance['description'];

		echo $args['before_widget'];

		if (!empty($title))
			echo '<div class="mmd-title">'. $args['before_title'] . $title . $args['after_title'] .'</div>';

		if (!empty($description))
			echo '<div class="mmd-description">'. $description .'</div>';

		echo $this->mmdigest_signup_form($instance['audience'], $instance['promotion'], $instance['show_first_name'], $instance['show_last_name'], $instance['show_schedule'], $instance['btn_text']);

		echo $args['after_widget'];

	}
		
	public function form( $instance ) {
	
		$title = (isset($instance['title']))? $instance['title']: __('Title', 'mmd_widget_domain');
		$description = (isset($instance['description']))? $instance['description']: __('Descipription', 'mmd_widget_domain');
		$audience = (isset($instance['audience']))? $instance['audience']: __('Audience', 'mmd_widget_domain');
		$promotion = (isset($instance['promotion']))? $instance['promotion']: __('Promotion', 'mmd_widget_domain');
		$show_first_name = (isset($instance['show_first_name']))? $instance['show_first_name']: __('First name', 'mmd_widget_domain');
		$show_last_name = (isset($instance['show_last_name']))? $instance['show_last_name']: __('Last name', 'mmd_widget_domain');
		$show_schedule = (isset($instance['show_schedule']))? $instance['show_schedule']: __('Schedule', 'mmd_widget_domain');
		$btn_text = (isset($instance['btn_text']))? $instance['btn_text']: __('Get Free Stuff', 'mmd_widget_domain');

		// Widget admin form
		?>
		<div class="widget-content">
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title (optional)' ); ?></label><br>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description (optional)' ); ?></label><br>
				<textarea class="widefat" rows="10" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo esc_attr( $description ); ?></textarea>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'audience' ); ?>"><?php _e( 'Audience' ); ?></label><br>
				<select class='widefat' id="<?php echo $this->get_field_id( 'audience' ); ?>" name="<?php echo $this->get_field_name( 'audience' ); ?>" type="text">
					<? 
					$mm_audiences = mmdigest_getMadMimiAudiences(); 
					foreach ($mm_audiences as $mm_audience):
					?>
					<option value="<?php echo $mm_audience['id']; ?>" <?php echo ($mm_audience['id']==$audience)? 'selected="selected"':'' ; ?>><?php echo $mm_audience['name']; ?></option>
					<?php endforeach; ?>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'promotion' ); ?>"><?php _e( 'Welcome Promotion' ); ?></label><br>
				<select class='widefat' id="<?php echo $this->get_field_id( 'promotion' ); ?>" name="<?php echo $this->get_field_name( 'promotion' ); ?>" type="text">
					<option value="0" <?php echo ($mm_promotion['id'] == 0) ? 'selected="selected"':'' ; ?>>* Do not Welcome</option>
					<? 
					$mm_promotions = mmdigest_getMadMimiPromotions(); 
					foreach ($mm_promotions as $mm_promotion):
					?>
					<option value="<?php echo $mm_promotion['id']; ?>" <?php echo ($mm_promotion['id']==$promotion)? 'selected="selected"':'' ; ?>><?php echo $mm_promotion['name']; ?></option>
					<?php endforeach; ?>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'btn_text' ); ?>"><?php _e( 'Subscribe button text (optional)' ); ?></label><br>
				<input class="widefat" id="<?php echo $this->get_field_id( 'btn_text' ); ?>" name="<?php echo $this->get_field_name( 'btn_text' ); ?>" type="text" value="<?php echo esc_attr( $btn_text ); ?>" />
			</p>
			<p>
				<input type="checkbox" name="<?php echo $this->get_field_name( 'show_first_name' ); ?>" id="<?php echo $this->get_field_id( 'show_first_name' ); ?>" class="checkbox"  <?php echo ($show_first_name === 'on')? 'checked="checked"':'' ; ?>>
				<label for="<?php echo $this->get_field_id( 'show_first_name' ); ?>">Show first name</label>
			</p>
			<p>
				<input type="checkbox" name="<?php echo $this->get_field_name( 'show_last_name' ); ?>" id="<?php echo $this->get_field_id( 'show_last_name' ); ?>" class="checkbox" <?php echo ($show_last_name === 'on')? 'checked="checked"':'' ; ?>>
				<label for="<?php echo $this->get_field_id( 'show_last_name' ); ?>">Show last name</label>
			</p>
			<p>
				<input type="checkbox" name="<?php echo $this->get_field_name( 'show_schedule' ); ?>" id="<?php echo $this->get_field_id( 'show_schedule' ); ?>" class="checkbox" <?php echo ($show_schedule === 'on')? 'checked="checked"':'' ; ?>>
				<label for="<?php echo $this->get_field_id( 'show_schedule' ); ?>">Show schedule</label>
			</p>
		</div>
		<?php 
	
	}
	
	public function update($new_instance, $old_instance) {
		
		$instance = array();
		$instance['title'] = (!empty($new_instance['title']))? strip_tags($new_instance['title']): '';
		$instance['description'] = (!empty($new_instance['description']))? $new_instance['description']: '';
		$instance['audience'] = $new_instance['audience'];
		$instance['btn_text'] = $new_instance['btn_text'];
		$instance['show_first_name'] = $new_instance['show_first_name'];
		$instance['show_last_name'] = $new_instance['show_last_name'];
		$instance['show_schedule'] = $new_instance['show_schedule'];

		return $instance;
	}
	
	function mmdigest_signup_form($audience, $promotion, $show_first_name, $show_last_name, $show_schedule, $btn_text){

		// Get subscribe button text
		$default_btn_text = 'Get Free Stuff';
		if ($btn_text != '') {
			$default_btn_text = $btn_text;
		}

		$out = '';
		$out .= '<div class="mmd-form">';
		$out .= '<form method="post" id="mmdigest_subscribe">';
		
		$out .= '<div class="mmd-form-email">';
		$out .= '<label for="mmd_email">Email <span class="required mmd-form-required" title="This field is required">*</span></label>';
		$out .= '<input id="email" name="mmd_subscribe[email]" type="email" value="" placeholder="Email" required/>';
		$out .= '</div>';
		
		if ($show_first_name) {
			$out .= '<div class="mmd-form-firstname">';
			$out .= '<label for="mmd_firstname">First name</label>';
			$out .= '<input id="mmd_firstname" name="mmd_subscribe[firstname]" type="text" value="" placeholder="First Name"/>';
			$out .= '</div>';
		}
		
		if ($show_last_name) {
			$out .= '<div class="mmd-form-lastname">';
			$out .= '<label for="mmd_lastname">Last name</label>';
			$out .= '<input id="mmd_lastname" name="mmd_subscribe[lastname]" type="text" value="" placeholder="Last Name"/>';
			$out .= '</div>';
		}

		if ($show_schedule) {
			$out .= '<div class="mmd-form-schedule">';
			$out .= '<label for="mmd_schedule">Select schedule</label>';
			$out .= '<select name="mmd_subscribe[schedule]" id="mmd_schedule">';
				$out .= '<option value="daily" selected>Every day</option>';
				$out .= '<option value="weekly">Weekly</option>';
				$out .= '<option value="instant">Instant</option>';
			$out .= '</select>';
			$out .= '</div>';
		}
			
		$out .= '<div class="mmd-form-submit">';
		$out .= '<input name="mmd_subscribe[submit]" class="button" type="submit" value="' . $default_btn_text . '" />';
		$out .= '</div>';
		
		$out .= '<input name="mmd_subscribe[audience]" type="hidden" value="' . $audience . '" />';
		$out .= '<input name="mmd_subscribe[promotion]" type="hidden" value="' . $promotion . '" />';
		
		$out .=	'</form>';
		$out .= '</div>';
        
        return $out;
		
    }
	
} // Class mmd_widget ends here


function mmd_load_widget() {
	register_widget( 'mmd_widget' );
}
add_action( 'widgets_init', 'mmd_load_widget' );